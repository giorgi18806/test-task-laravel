@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-between">
        <div style="font-size: 2.5rem; font-weight: 700; color: #007bff;">{{ __('Dashboard') }}</div>

        <div class="container-fluid">
            <div class="box-header row mx-0 d-flex justify-content-between">
                <div>
                    <h4><strong>USERS</strong></h4>
                </div>
                <div class="justify-content-end">
                    <div class="row">
                        <a href="{{ route( 'user.create') }}" class="btn btn-primary mr-4 mb-2">
                            <i class="fa fa-plus-square-o"></i> Создать пользователя
                        </a>
                    </div>
                </div>
            </div>
            <table class="table table-striped" >
                <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Create_at</th>
                <th>Действие</th>
                </thead>
                <tbody>
                    @forelse ($users as $user)
                        <tr>
                            <td><span>{{ $user->id }}</span></td>
                            <td><span>{{ $user->name }}</span></td>
                            <td><span>{{ $user->email }}</span></td>
                            <td><span>{{ $user->created_at }}</span></td>
                            <td>
                                <span>
                                    <div class="btn-group" role="group">
                                        <a href="{{ route( 'user.edit', $user->id) }}" class="btn btn-warning">Редактировать</a>

                                        <form onsubmit="if(confirm('Удалить')){return true}else{return false}" action="{{ route( 'user.destroy', $user->id) }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Удалить</button>
                                        </form>
                                    </div>
                                </span>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3" class="text-center">
                                <h2>Данные отсутствуют</h2>
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
