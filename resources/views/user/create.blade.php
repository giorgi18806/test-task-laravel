@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-between">
            <div style="font-size: 2.5rem; font-weight: 700; color: #007bff;"><a href="{{ route('home') }}">{{ __('Dashboard') }}</a></div>
            <div class="box-header row mx-0 d-flex justify-content-between">
                <h4 class="mt-4"><strong>Create new user</strong></h4>
            </div>
            <div class="container-fluid">
                <form action="{{route('user.store')}}" method="post">
                    @csrf
                    <div class="mt-5 row">
                        <div class="form-group col-4">
                            <h4>Name</h4>
                            <input type="title" class="form-control" name="name" required autofocus>
                        </div>
                        <div class="form-group col-4">
                            <h4>Email</h4>
                            <input type="email" name="email" class="form-control" required>
                        </div>
                        <div class="form-group col-4">
                            <h4>Password</h4>
                            <input type="password" name="password" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row justify-content-center">
                        <div class="col-sm-5 mt-5">
                            <button type="submit" class="btn btn-lg btn-primary btn-block">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
